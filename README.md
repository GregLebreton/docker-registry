# DOCKER REGISTRE AVEC UI

Ce projet a pour but d'instancier un registre Docker sécurisé par mot de passe et 
accessible via une UI. Votre Docker Hub!

### CREATION D'UN PASSWORD:

Ici, remplacer testuser et testpassword par le nom d'utilisateur et le mot de passe désiré.
```sh
sudo apt-get install apache2-utils
htpasswd -Bbn testuser testpassword > auth/htpasswd
```

### METTRE A JOUR LES INFOS DNAS LE DOCKER-COMPOSE:
```sh
nano docker-compose.yml
```

### LANCER LA STACK
```sh
docker-compose up -d
```
